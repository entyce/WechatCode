# 仓库目录介绍
~~~
WeCHatCode
├─aopdemo
│  
├─ExcelDemo 使用EasyExcel导入导出Excel
│  
├─LoadBean Bean的三种加载方式
│  │  
│  ├─auto_load  自动加载
│  │  
│  ├─java_load  Java代码加载
│  │  
│  └─xml_load   XML文件加载
│      
├─Mybatis_Generator MyBatis代码生成插件的使用
│  
├─Shiro   Shiro相关
│  └─shiro_ini ini配置文件demo
│      
├─ssmapi  写API
│               
└─ssmdemo 基础SSM Demo
~~~



## 如果我的文章对你有用就来一波关注吧

![公众号二维码](https://mmbiz.qpic.cn/mmbiz_jpg/xCxowSvVWUb2GQnFNdABImqOn8eRC5GdkRc7OhxZMlJ0wgUEWWiaQicnN5EcbAoC1I6AfnTibUkIzSNp12EDkGksA/640?wx_fmt=jpeg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)